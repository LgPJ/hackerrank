import java.util.Scanner;

public class BuclesII {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese Q:");
        int t = in.nextInt();
        int resultado = 0;
        int aux = 0;
        int aux2 = 0;

        for (int i = 0; i < t; i++) {
            System.out.print("Ingrese A:");
            int a = in.nextInt();

            System.out.print("Ingrese B:");
            int b = in.nextInt();

            System.out.print("Ingrese N:");
            int n = in.nextInt();
            
            for (int j = 0; j < n; j++) {
                
                if(j == 0){
                    aux = (int) (a + Math.pow(2, j) * b);
                    resultado = aux;
                }else{
                    aux2 = (int) Math.pow(2, j) * b;
                }
                resultado = resultado + aux2;
                System.out.print(resultado + " ");
            }
            System.out.println();
            aux = 0;
            resultado = 0;
            aux2 = 0;
        }
    
        in.close();
    }
}

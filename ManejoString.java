import java.util.Scanner;

public class ManejoString {

    public static String upperCaseFirst(String cad) {

        String firstLtr = cad.substring(0, 1);
        String restLtrs = cad.substring(1, cad.length());
      
        firstLtr = firstLtr.toUpperCase();
        cad = firstLtr + restLtrs;
        return cad;
    }

    public static String lexografia(String cad, String cad2) {
        
        if(cad.compareTo(cad2) < 0){
            return "Yes";
        } else{
            return "No";
        }
    }

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese cadena 1: ");
        String cad = sc.next();
        System.out.println("Ingrese cadena 2: ");
        String cad2 = sc.next();

        int sumaCad = cad.length() + cad2.length();
        System.out.println(sumaCad);
        
        String result = upperCaseFirst(cad);
        String result2 = upperCaseFirst(cad2);
        System.out.println(lexografia(cad, cad2));
        System.out.printf("%s %s",result, result2);
        
        sc.close();
    }
}

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class JuegoEnMatriz {

    public static boolean canWin(int leap, int[] game) {
        return isSolvable(leap, game, 0);
    }

    private static boolean isSolvable(int m, int[] arr, int i) {
    if (i < 0 || arr[i] == 1) return false;
    if ((i == arr.length - 1) || i + m > arr.length - 1) return true;
    
    arr[i] = 1;
    return isSolvable(m, arr, i + 1) || isSolvable(m, arr, i - 1) || isSolvable(m, arr, i + m);
    }
    public static void main(String[] args) throws IOException {

        File f = new File("file.txt");
        if (!f.exists()) {
            f.createNewFile();
        }

        try (Scanner sc = new Scanner(f);) {

            int q = sc.nextInt();
            for (int i = 1; i <= q; i++) {

                int tam = sc.nextInt();
                int lep = sc.nextInt();
                int[] numeros = new int[tam];

                for (int ind = 0; ind < numeros.length; ind++) {
                    numeros[ind] = sc.nextInt();
                    // System.out.printf(" %d ", numeros[ind]);
                }
                System.out.println( (canWin(lep, numeros)) ? "YES" : "NO" );
            }

        } catch (

        Exception e) {
            e.printStackTrace();
        }

    }
}

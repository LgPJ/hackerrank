import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Matriz1D {
    public static void main(String[] args) throws IOException {

        File f = new File("file.txt");
        if (!f.exists()) {
            f.createNewFile();
        }
        

        try (Scanner sc = new Scanner(f);) {

            int n = sc.nextInt();
            int[] numeros = new int[n];

            for (int i = 0; i < numeros.length; i++) {
                System.out.println("Ingrese datos a almacenar:");
                int datos = sc.nextInt();
                numeros[i] = datos;
            }
            System.out.println("=================================");
            for (int i = 0; i < numeros.length; i++) {
                System.out.println(numeros[i]);
            }

        } catch(Exception e){
            e.printStackTrace();
        }
       
    }
}

import java.util.Scanner;

public class NumerosRaros {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int numero;
        boolean validar = true;

        do{ 
            System.out.println("Ingrese numero a verificar:");
            numero = sc.nextInt();

            if(1 <= numero && numero <= 100){
                if (numero % 2 != 0) {
                    System.out.println("WEIRD IMPAR");
                }
                if (numero % 2 == 0) {
                    if (2 <= numero && numero <= 5) {
                        System.out.println("NOT WIRED CON UNIFORME");
                    } else if (6 <= numero && numero <= 20) {
                        System.out.println("WEIRD CON UNIFORMES");
                    } else if (numero > 20) {
                        System.out.println("NOT WIRED CON UNIFORMES Y MAYOR QUE 20");
                    }
                }
                validar = true;
            }else{
                validar = false;
            }
        }while(validar != true);  
        sc.close();
    }
}

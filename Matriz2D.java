import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Matriz2D {
    public static void main(String[] args) throws IOException {

        File f = new File("file.txt");
        if (!f.exists()) {
            f.createNewFile();
        }

        try (Scanner sc = new Scanner(f)) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] matriz = new int[n][m];

            // Llenado de datos dentro de la matriz, datos provenientes de archivo
            for (int i = 0; i <= n - 1; i++) {
                for (int j = 0; j <= m - 1; j++) {
                    matriz[i][j] = sc.nextInt();
                }
            }
        
            /*Cilco para imprimir la matriz que se ha cargador por archivo*/
            for (int i = 0; i <= n - 1; i++) {
                System.out.println();
                for (int j = 0; j <= m - 1; j++) {
                    System.out.printf(" %d ", matriz[i][j]);
                }
            }

            int datos = 0;
            int maximo = 0;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    datos = matriz[i][j] + matriz[i][j + 1] + matriz[i][j + 2] + matriz[i + 1][j + 1] + matriz[i + 2][j]
                            + matriz[i + 2][j + 1] + matriz[i + 2][j + 2];
                    if (datos > maximo) {
                        maximo = datos;
                    }
                }
            }
            System.out.println(maximo);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

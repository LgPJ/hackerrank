import java.util.Scanner;

public class Anagrama {

    static boolean ret = false;

    static boolean isAnagram(String a, String b) {

        if (a.length() == b.length()) {
            for (int i = 0; i < a.length(); i++) {

                if (!b.contains(String.valueOf(b.charAt(i)))) {
                    return ret = false;
                }
            }
                return ret = true;
        } else {
            return ret;
        }
        

    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese cadena 1:");
        String a = scan.next();
        System.out.println("Ingrese cadena 2:");
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println((ret) ? "Anagrams" : "Not Anagrams");
    }
}

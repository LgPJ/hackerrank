import java.util.Scanner;

public class FormatoPrintf {
    
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        System.out.println("================================");
        for(int i = 0; i < 3; i++){

            System.out.println("Ingrese la cadena:");
            String cadena = sc.next();
            System.out.println("Ingrese el numero:");
            int numero = sc.nextInt();

            System.out.printf("%-15s %03d %n", cadena, numero);
        }
        System.out.println("================================");
        sc.close();
    }
}

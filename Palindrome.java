import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese cadena: ");
        String cadena = sc.nextLine();
        int i = 0;
        char[] caracteres = cadena.toCharArray();
        int n = caracteres.length - 1;
        boolean valido = true;
        do {
            if (caracteres.length == 0) {
                valido = true;
            } else {
                char letraOne = caracteres[i];
                char letraN = caracteres[n];
                if(letraOne == letraN) {
                    i++;
                    n--;
                } else {
                    valido = false;
                }
            }
        } while (valido && i <= n);

        if(valido){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }

        sc.close();
    }
}

import java.util.Scanner;

public class EndOfFile {
    
    public static void main(String[] args){

        /** Este ejercicio es para comprobar el EOF End Of File.
         * el ciclo while deja ser verdaderp cuando el metodo hasNext se vuelve falso y asi
         * que en entendido que ha llegado al EOF de ese dato
        */
        Scanner sc = new Scanner(System.in);
        int i = 0;

        while(sc.hasNext()){
            System.out.println(++i + " " + sc.nextLine());
        }

        sc.close();

    }
}

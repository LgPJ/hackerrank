import java.util.Scanner;

public class BloqueStatic {
    
    static Scanner sc = new Scanner(System.in);
    static int B;
    static int H;
    static boolean flag = true;

    static {
        System.out.println("Ingrese BASE:");
        B = sc.nextInt();
        System.out.println("Ingrese ALTURA");
        H = sc.nextInt();

        if (B <= 0 || H <= 0) { 
            System.out.println("java.lang.Exception: Breadth and height must be positive");
            flag = false;
        }
    }

    public static void main(String[] args) {        
        if (BloqueStatic.flag) {
            int area = B * H;
            System.out.print("El area es: " + area);
        }
    }
}
